import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

export default function Message({ message, currentUserId, accessToken }) {
  const history = useHistory();
  const [user, setUser] = useState(undefined);

  useEffect(() => {
    if (!accessToken) {
      history.push('/login');
    }

    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/users/${message.senderUserId}`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          return;
        }

        if (content.success) {
          setUser(content.user);
        }
      });
  }, []);

  return (
    <div className={`Message ${currentUserId === message.senderUserId.toString() ? 'sent' : 'received'}`}>
      <div className="content">
        <p className="text">{message.text}</p>
        <p className="username">{currentUserId === message.senderUserId.toString() ? 'you' : `${user ? user.nickName : ''}` }</p>
      </div>
    </div>
  );
}
