import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import TeamMemberItem from './TeamMemberItem';
import Chat from './Chat';
import './team.scss';

export default function TeamChat({ accessToken, userId }) {
  const history = useHistory();
  const [users, setUsers] = useState([]);
  const [receiverUserId, setReceiverUserId] = useState(undefined);

  useEffect(() => {
    if (!accessToken) {
      history.push('/login');
    }

    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/users`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          return;
        }

        if (content.success) {
          setUsers(content.users);
        }
      });
  }, []);

  const handleTeamMemberClicked = (id) => {
    setReceiverUserId(id);
  };

  return (
    <div className="TeamChat">
      <h1>Start a chat with a team mate!</h1>
      <div className="columns-container">
        <div className="TeamMembersList">
          <h4>Team members</h4>
          {
            users.map((user) => {
              if (user.id === parseInt(userId, 10)) {
                return '';
              }

              return <TeamMemberItem key={user.id} user={user} onTeamMemberItemClicked={handleTeamMemberClicked} />;
            })
          }
        </div>

        {!receiverUserId ? '' : <Chat key={receiverUserId} senderUserId={userId} receiverUserId={receiverUserId} accessToken={accessToken} />}
      </div>
    </div>
  );
}
