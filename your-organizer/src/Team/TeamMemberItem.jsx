import React from 'react';

export default function TeamMemberItem({ user, onTeamMemberItemClicked }) {
  return (
    // eslint-disable-next-line jsx-a11y/click-events-have-key-events
    <div className="TeamMemberItem" onClick={() => onTeamMemberItemClicked(user.id)} role="button" tabIndex="0">
      { user.firstName }
      {' '}
      -
      {' '}
      { user.nickName }
    </div>
  );
}
