import React, { useEffect, useState } from 'react';
import io from 'socket.io-client';
import Message from './Message';

export default function Chat({ senderUserId, receiverUserId, accessToken }) {
  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState('');
  const [socket, setSocket] = useState(undefined);
  const [chatContainerElement, setChatContainerElement] = useState(undefined);

  const receiveMessageHandler = ({ text, senderId }) => {
    setMessages([...messages, { text, senderUserId: senderId }]);
  };

  useEffect(() => {
    if (!socket) {
      setSocket(io(process.env.REACT_APP_API_URL, { query: { senderUserId } }));
    } else {
      socket.on('receive-message', receiveMessageHandler);
    }

    if (chatContainerElement) {
      chatContainerElement.scrollTop = chatContainerElement.scrollHeight;
    }
  }, [socket, messages, chatContainerElement]);

  useEffect(() => {
    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/messages?senderUserId=${senderUserId}&receiverUserId=${receiverUserId}`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          return;
        }

        if (content.success) {
          setMessages(content.messages);
        }
      });

    if (!socket) {
      return;
    }

    // eslint-disable-next-line consistent-return
    return () => socket.off('message', receiveMessageHandler);
  }, []);

  const sendMessage = (event) => {
    event.preventDefault();

    socket.emit('send-message', { message: newMessage, receiverUserId });
    setMessages([...messages, { text: newMessage, senderUserId }]);
    setNewMessage('');
  };

  return (
    <div className="Chat">
      <div className="messages-container" ref={(el) => setChatContainerElement(el)}>
        { messages.map((message) => <Message message={message} currentUserId={senderUserId} key={messages.indexOf(message)} accessToken={accessToken} />) }
      </div>

      <form onSubmit={(event) => sendMessage(event)}>
        <input type="text" placeholder="Type your message here..." value={newMessage} onChange={(event) => setNewMessage(event.target.value)} />
        <button type="submit" disabled={newMessage.length === 0}>Send</button>
      </form>
    </div>
  );
}
