import React, { useState, useEffect } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Navbar from '../components/layout/Navbar';
import Dashboard from '../Dashboard/Dashboard';
import Tasks from '../Tasks/Tasks';
import Notes from '../Notes/Notes';
import TeamChat from '../Team/TeamChat';
import Login from '../Auth/Login';
import Signup from '../Auth/Signup';
import ResetPassword from '../Auth/ResetPassword';
import NewPassword from '../Auth/NewPassword';
import NotePage from '../Notes/NotePage';
import TaskPage from '../Tasks/TaskPage';
import './App.scss';

function App() {
  const [accessToken, setAccessToken] = useState(undefined);
  const [userId, setUserId] = useState(undefined);
  const [accessTokenIsLoaded, setAccessTokenIsLoaded] = useState(false);

  useEffect(() => {
    if (localStorage.getItem('accessToken')) {
      setAccessToken(localStorage.getItem('accessToken'));
    }

    if (localStorage.getItem('userId')) {
      setUserId(localStorage.getItem('userId'));
    }

    setAccessTokenIsLoaded(true);
  }, []);

  const handleLogout = () => {
    setAccessToken(undefined);
    setUserId(undefined);

    localStorage.removeItem('accessToken');
    localStorage.removeItem('userId');
  };

  const handleUserAuth = ({ token, id }) => {
    setAccessToken(token);
    setUserId(id);

    localStorage.setItem('accessToken', token);
    localStorage.setItem('userId', id);
    setAccessTokenIsLoaded(true);
  };

  return (
    <div className="App">
      {accessToken ? <Navbar onLogoutButtonClicked={handleLogout} /> : ''}

      <div className="page-container">
        {
          !accessTokenIsLoaded ? '' : (
            <Switch>
              <Route exact path="/" component={() => (accessToken ? <Dashboard accessToken={accessToken} userId={userId} /> : <Redirect to="/login" />)} />
              <Route path="/tasks" component={() => (accessToken ? <Tasks accessToken={accessToken} userId={userId} /> : <Redirect to="/login" />)} />
              <Route path="/task/:taskId" component={() => (accessToken ? <TaskPage accessToken={accessToken} /> : <Redirect to="/login" />)} />
              <Route path="/notes" component={() => (accessToken ? <Notes accessToken={accessToken} userId={userId} /> : <Redirect to="/login" />)} />
              <Route path="/note/:noteId" component={() => (accessToken ? <NotePage accessToken={accessToken} /> : <Redirect to="/login" />)} />
              <Route path="/team-chat" component={() => (accessToken ? <TeamChat accessToken={accessToken} userId={userId} /> : <Redirect to="/login" />)} />
            </Switch>
          )
        }
      </div>

      <div className="full-width-page-container">
        <Switch>
          <Route
            path="/login"
            component={() => <Login onUserAuth={(userObject) => handleUserAuth(userObject)} accessToken={accessToken} />}
          />
          <Route path="/sign-up" component={() => <Signup accessToken={accessToken} />} />
          <Route path="/reset-password" component={() => <ResetPassword />} />
          <Route path="/new-password/:token" component={() => <NewPassword />} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
