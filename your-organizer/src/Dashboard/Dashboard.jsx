import React, { useEffect, useState } from 'react';
import SummaryCard from './components/SummaryCard';
import './dashboard.scss';
import RecentTasksList from '../Tasks/components/RecentTasksList';
import RecentNotesList from '../Notes/components/RecentNotesList';

export default function Dashboard({ accessToken, userId }) {
  const [deadlinesToday, setDeadlinesToday] = useState(0);
  const [todos, setTodos] = useState(0);
  const [ongoing, setOngoing] = useState(0);
  const [done, setDone] = useState(0);

  useEffect(() => {
    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    if (!userId) {
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/tasks/dashboard-count/${userId}`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          return;
        }

        if (content.success) {
          setDeadlinesToday(content.deadlinesToday);
          setTodos(content.todos);
          setOngoing(content.ongoing);
          setDone(content.done);
        }
      });
  }, [userId]);

  return (
    <div>
      <h1>Your tasks overview</h1>

      <div className="cards-flex-container">
        <SummaryCard title="Deadlines for today" number={deadlinesToday} />
        <SummaryCard title="To do" number={todos} />
        <SummaryCard title="Ongoing" number={ongoing} />
        <SummaryCard title="Done" number={done} />
      </div>

      <div className="lists-flex-container">
        <RecentTasksList accessToken={accessToken} />
        <RecentNotesList accessToken={accessToken} userId={userId} />
      </div>
    </div>
  );
}
