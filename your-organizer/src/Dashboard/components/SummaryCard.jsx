import React from 'react';

export default function SummaryCard({ title, number }) {
  return (
    <div className="SummaryCard">
      <h2>{title}</h2>
      <p>{number}</p>
    </div>
  );
}
