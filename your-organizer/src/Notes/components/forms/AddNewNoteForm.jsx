import React, { useState } from 'react';
import '../../notes.scss';
import '../../../Tasks/tasks.scss';

export default function AddNewNoteForm({ onCloseButtonClicked, accessToken, userId }) {
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [successMessage, setSuccessMessage] = useState('');

  const addNote = (event) => {
    event.preventDefault();
    if (!title || !content) {
      setErrorMessage('Please fill in title and content!');
      return;
    }

    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/notes`, {
      method: 'POST',
      headers,
      body: JSON.stringify({
        title,
        content,
        userId,
      }),
    }).then((response) => response.json())
      .then((body) => {
        if (body.error) {
          setErrorMessage(content.errorMessage);
          return;
        }

        if (body.success) {
          setSuccessMessage('Note was successfully created!');
          setTitle('');
          setContent('');
          onCloseButtonClicked();
        }
      });
  };

  return (
    <form className="AddNewNoteForm form" onSubmit={addNote}>
      <div>
        <button type="button" className="close-button" onClick={onCloseButtonClicked}>x</button>
        <h3>Add a new note</h3>
      </div>

      {errorMessage ? <div className="error">{errorMessage}</div> : ''}
      {successMessage ? <div className="success">{successMessage}</div> : ''}

      <input type="text" id="title" placeholder="Title" value={title} onChange={(e) => setTitle(e.target.value)} />
      <textarea name="content" id="content" cols="30" rows="10" placeholder="Content" value={content} onChange={(e) => setContent(e.target.value)} />

      <button type="submit" className="is-bright" onClick={addNote}>Add</button>
    </form>
  );
}
