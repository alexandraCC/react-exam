import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import NoteItem from './items/NoteItem';
import '../notes.scss';

export default function RecentNotesList({ accessToken, userId }) {
  const history = useHistory();
  const [notes, setNotes] = useState();

  useEffect(() => {
    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/notes?userId=${userId}`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          return;
        }

        if (content.success) {
          setNotes(content.notes);
        }
      });
  }, []);

  return (
    <div className="RecentNotesList">
      <h2>Notes</h2>

      <div className="list-container">
        {notes ? (notes.slice(0, 4).map((note) => <NoteItem note={note} key={note.id} accessToken={accessToken} />)) : ''}
      </div>

      <button type="submit" onClick={() => history.push('/notes')}>See all</button>
    </div>
  );
}
