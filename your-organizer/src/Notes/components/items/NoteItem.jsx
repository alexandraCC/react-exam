import React from 'react';
import { useHistory } from 'react-router-dom';

export default function NoteItem({ note }) {
  const history = useHistory();

  return (
    // eslint-disable-next-line jsx-a11y/click-events-have-key-events
    <div className="NoteItem" role="button" onClick={() => history.push(`note/${note.id}`)} tabIndex="0">
      <p>{note ? note.title : ''}</p>
      <p>{note ? `${new Date(note.timeCreated).getDate()} / ${new Date(note.timeCreated).getMonth() + 1} / ${new Date(note.timeCreated).getFullYear()}` : ''}</p>
    </div>
  );
}
