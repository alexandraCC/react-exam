import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import AddNewNoteForm from './components/forms/AddNewNoteForm';
import NoteItem from './components/items/NoteItem';
import './notes.scss';

export default function Notes({ accessToken, userId }) {
  const [showAddNotesForm, setShowAddNotesForm] = useState(false);
  const [notes, setNotes] = useState([]);
  const history = useHistory();

  useEffect(() => {
    if (!accessToken) {
      history.push('/login');
    }

    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/notes?userId=${userId}`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          return;
        }

        if (content.success) {
          setNotes(content.notes);
        }
      });
  }, [showAddNotesForm]);

  return (
    <div className="Notes">
      {showAddNotesForm ? <AddNewNoteForm onCloseButtonClicked={() => { setShowAddNotesForm(false); }} accessToken={accessToken} userId={userId} /> : '' }

      <div className={`page-content ${showAddNotesForm || showAddNotesForm ? 'blur' : ''}`}>
        <div className="flex-container">
          <h1>Notes</h1>
          <button type="submit" onClick={() => { setShowAddNotesForm(true); }}>+ Add note</button>
        </div>
        <div className="notes-list">
          {notes.map((note) => <NoteItem key={note.id} note={note} />)}
        </div>
      </div>
    </div>
  );
}
