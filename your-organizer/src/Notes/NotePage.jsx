import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';

export default function NotePage({ accessToken }) {
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [noteHasBeenLoaded, setNoteHaBeenLoaded] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [successMessage, setSuccessMessage] = useState('');
  const history = useHistory();
  const { noteId } = useParams();

  useEffect(() => {
    if (!accessToken) {
      history.push('/login');
    }

    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/notes/${noteId}`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((body) => {
        if (body.error) {
          return;
        }

        if (body.success) {
          setTitle(body.note.title);
          setContent(body.note.content);
          setNoteHaBeenLoaded(true);
        }
      });
  }, []);

  const updateNote = (event) => {
    event.preventDefault();

    if (!title || !content) {
      setErrorMessage('Title and content cannot be empty');
      return;
    }

    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/notes/${noteId}`, {
      method: 'PATCH',
      headers,
      body: JSON.stringify({
        title,
        content,
      }),
    }).then((response) => response.json())
      .then((body) => {
        if (body.error) {
          setErrorMessage(content.errorMessage);
          return;
        }

        if (body.success) {
          setSuccessMessage('Note was successfully updated!');
        }
      });
  };

  const deleteNote = () => {
    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/notes/${noteId}`, {
      method: 'DELETE',
      headers,
    }).then((response) => response.json())
      .then((body) => {
        if (body.error) {
          return;
        }

        if (body.success) {
          history.push('/notes');
        }
      });
  };

  return (
    <div className="NotePage">
      {errorMessage ? <div className="error">{errorMessage}</div> : ''}
      {successMessage ? <div className="success">{successMessage}</div> : ''}

      { !noteHasBeenLoaded ? ''
        : (
          <div>
            <input
              className="title-input"
              type="text"
              id="title"
              placeholder="Title"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />

            <textarea
              className="display-textarea"
              name="content"
              id="content"
              cols="150"
              rows="30"
              placeholder="Content"
              value={content}
              onChange={(e) => setContent(e.target.value)}
            />

            <div className="buttons-container">
              <button type="button" onClick={updateNote}>Update</button>
              <button type="button" className="delete" onClick={deleteNote}>x Delete</button>
            </div>
          </div>
        )}
    </div>
  );
}
