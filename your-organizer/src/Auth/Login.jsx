import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import './auth.scss';

export default function Login({ onUserAuth, accessToken }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const history = useHistory();

  useEffect(() => {
    if (accessToken) {
      history.push('/');
    }
  }, []);

  const authenticateUser = (event) => {
    event.preventDefault();

    if (!email || !password) {
      setErrorMessage('Please fill in all of the fields!');
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/auth/login`, {
      method: 'POST',
      body: JSON.stringify({
        email,
        password,
      }),
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          setErrorMessage(content.errorMessage);
          return;
        }

        if (content.success) {
          onUserAuth({ token: content.accessToken, id: content.userId });
          history.push('/');
        }
      });

    setEmail('');
    setPassword('');
  };

  return (
    <div className="Login">
      <form className="form-container" onSubmit={(event) => authenticateUser(event)}>
        <h1>Login</h1>

        {errorMessage ? <div className="error">{errorMessage}</div> : ''}

        <div className="form-contents">
          <input type="text" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />
          <input type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />

          <p className="help-text">
            Do not have an account? Sign up
            {' '}
            <a href="/sign-up">here</a>
            .
            Forgot your password? Reset it
            {' '}
            <a href="/reset-password">here</a>
            .
          </p>

          <button type="submit">Login</button>
        </div>
      </form>
    </div>
  );
}
