import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import './auth.scss';

export default function Signup({ accessToken }) {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [successMessage, setSuccessMessage] = useState('');
  const history = useHistory();

  useEffect(() => {
    if (accessToken) {
      history.push('/');
    }
  });

  const signUpUser = (event) => {
    event.preventDefault();
    if (!firstName || !lastName || !password || !email) {
      setErrorMessage('Please fill in all of the fields!');
      return;
    }

    if (!email.match(/@/)) {
      setErrorMessage('Please enter a valid email address!');
      return;
    }

    if (password.length < 8) {
      setErrorMessage('Password needs to be at least 8 characters!');
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/auth/sign-up`, {
      method: 'POST',
      body: JSON.stringify({
        firstName,
        lastName,
        email,
        password,
      }),
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          setErrorMessage(content.errorMessage);
          return;
        }

        if (content.success) {
          setSuccessMessage('User was successfully created! Check your mail to validate yourself!');
        }
      });

    setFirstName('');
    setLastName('');
    setEmail('');
    setPassword('');
  };

  return (
    <div className="Signup">
      <form className="form-container" onSubmit={(event) => signUpUser(event)}>
        <h1>Signup</h1>

        {errorMessage ? <div className="error">{errorMessage}</div> : ''}
        {successMessage ? <div className="success">{successMessage}</div> : ''}

        <div className="form-contents">
          <input
            type="text"
            placeholder="First name"
            value={firstName}
            onChange={(event) => { setFirstName(event.target.value); }}
          />

          <input
            type="text"
            placeholder="Last name"
            value={lastName}
            onChange={(event) => { setLastName(event.target.value); }}
          />

          <input
            type="text"
            placeholder="Email"
            value={email}
            onChange={(event) => { setEmail(event.target.value); }}
          />

          <input
            type="password"
            placeholder="Password"
            value={password}
            onChange={(event) => { setPassword(event.target.value); }}
          />
          <p className="help-text">The password needs to be a minimum of 8 characters</p>
          <p className="help-text">
            Have an account already? Login
            {' '}
            <a href="/login">here</a>
            .
          </p>
          <button type="submit">Signup</button>
        </div>
      </form>
    </div>
  );
}
