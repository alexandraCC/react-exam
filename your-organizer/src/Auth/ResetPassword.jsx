import React, { useState } from 'react';
import './auth.scss';

export default function ResetPassword() {
  const [email, setEmail] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [successMessage, setSuccessMessage] = useState('');

  const resetPassword = (event) => {
    event.preventDefault();
    if (!email) {
      setErrorMessage('Please fill in the email!');
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/auth/reset-password`, {
      method: 'POST',
      body: JSON.stringify({
        email,
      }),
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          setErrorMessage(content.errorMessage);
          return;
        }

        if (content.success) {
          setSuccessMessage(content.successMessage);
        }
      });

    setEmail('');
  };

  return (
    <div className="ResetPassword">
      <form className="form-container" onSubmit={(event) => resetPassword(event)}>
        <h1>Reset Password</h1>

        {errorMessage ? <div className="error">{errorMessage}</div> : ''}
        {successMessage ? <div className="success">{successMessage}</div> : ''}

        <div className="form-contents">
          <input type="text" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />

          <button type="submit">Send email to reset password</button>
        </div>
      </form>
    </div>
  );
}
