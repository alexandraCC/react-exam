import React from 'react';
import { Link } from 'react-router-dom';

export default function Navbar({ onLogoutButtonClicked }) {
  return (
    <nav>
      <Link to="/">YourOrganizer</Link>

      <div className="right-container">
        <Link to="/tasks">Tasks</Link>
        <Link to="/notes">Notes</Link>
        <Link to="/team-chat">Team Chat</Link>
        <button type="submit" onClick={onLogoutButtonClicked}>Logout</button>
      </div>
    </nav>
  );
}
