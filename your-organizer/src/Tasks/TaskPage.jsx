import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';

export default function TaskPage({ accessToken }) {
  const history = useHistory();
  const { taskId } = useParams();

  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [assignee, setAssignee] = useState('');
  const [project, setProject] = useState('');
  const [deadline, setDeadline] = useState(undefined);
  const [assignees, setAssignees] = useState(undefined);
  const [projects, setProjects] = useState(undefined);
  const [state, setState] = useState('');
  const [taskHasBeenLoaded, setTaskHaBeenLoaded] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [successMessage, setSuccessMessage] = useState('');

  useEffect(() => {
    if (!accessToken) {
      history.push('/login');
    }

    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/projects`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          return;
        }

        if (content.success) {
          setProjects(content.projects);
        }
      });

    fetch(`${process.env.REACT_APP_API_URL}/users`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          return;
        }

        if (content.success) {
          setAssignees(content.users);
        }
      });

    fetch(`${process.env.REACT_APP_API_URL}/tasks/${taskId}`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((body) => {
        if (body.error) {
          return;
        }

        if (body.success) {
          setTitle(body.task.title);
          setDescription(body.task.description);
          setAssignee(body.task.assigneeId ? body.task.assigneeId : '');
          setProject(body.task.projectId ? body.task.projectId : '');
          setDeadline(body.task.deadline ? `${new Date(body.task.deadline).getFullYear()}-${(`0${(new Date(body.task.deadline).getMonth() + 1)}`).slice(-2)}-${(`0${new Date(body.task.deadline).getDate()}`).slice(-2)}` : '');
          setState(body.task.state);
          setTaskHaBeenLoaded(true);
        }
      });
  }, []);

  const updateTask = (event) => {
    event.preventDefault();

    if (!title || !description) {
      setErrorMessage('Please fill in at least a title and description!');
      return;
    }

    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/tasks/${taskId}`, {
      method: 'PATCH',
      headers,
      body: JSON.stringify({
        title,
        description,
        assigneeId: assignee || null,
        projectId: project || null,
        deadline: deadline ? new Date(deadline).getTime() : null,
        state,
      }),
    }).then((response) => response.json())
      .then((body) => {
        if (body.error) {
          setErrorMessage(body.errorMessage);
          return;
        }

        if (body.success) {
          setSuccessMessage('Task was successfully updated!');
        }
      });
  };

  const deleteTask = () => {
    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/tasks/${taskId}`, {
      method: 'DELETE',
      headers,
    }).then((response) => response.json())
      .then((body) => {
        if (body.error) {
          return;
        }

        if (body.success) {
          history.push('/tasks');
        }
      });
  };

  return (
    <div className="TaskPage">

      {errorMessage ? <div className="error">{errorMessage}</div> : ''}
      {successMessage ? <div className="success">{successMessage}</div> : ''}

      { !taskHasBeenLoaded ? '' : (
        <div>
          <div className="task-page-containers">
            <div className="main-information">
              <input
                className="title-input"
                type="text"
                id="title"
                placeholder="Title"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />

              <textarea
                className="display-textarea"
                name="content"
                id="content"
                cols="100"
                rows="20"
                placeholder="Content"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </div>

            <div className="side-information">
              <div>Assigne</div>
              <select name="assignee" id="assignee" value={assignee} onChange={(e) => setAssignee(e.target.value)}>
                <option value="">Assignee</option>
                {!assignees ? '' : assignees.map((element) => <option value={element.id} key={element.id}>{`${element.nickName} - ${element.firstName} ${element.lastName}` }</option>)}
              </select>

              <div>Project</div>
              <select name="project" id="project" value={project} onChange={(e) => setProject(e.target.value)}>
                <option value="">Project</option>
                {!projects ? '' : projects.map((element) => <option value={element.id} key={element.id}>{element.name}</option>)}
              </select>

              <div>State</div>
              <select name="state" id="state" value={state} onChange={(e) => setState(e.target.value)}>
                <option value="toDo">To do</option>
                <option value="inProgress">In progress</option>
                <option value="done">Done</option>
              </select>

              <div>Deadline</div>
              <input type="date" value={deadline} onChange={(e) => setDeadline(e.target.value)} />
            </div>
          </div>

          <div className="buttons-container">
            <button type="button" onClick={updateTask}>Update</button>
            <button type="button" className="delete" onClick={deleteTask}>x Delete</button>
          </div>
        </div>
      )}
    </div>
  );
}
