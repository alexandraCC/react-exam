import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import './tasks.scss';
import ProjectItem from './components/items/ProjectItem';
import TaskStateCategory from './components/TaskStateCategory';
import AddNewTaskForm from './components/forms/AddNewTaskForm';
import AddNewProjectForm from './components/forms/AddNewProjectForm';

export default function Tasks({ accessToken, userId }) {
  const [showAddTaskForm, setShowAddTaskForm] = useState(false);
  const [showAddProjectForm, setShowAddProjectForm] = useState(false);
  const [tasks, setTasks] = useState([]);
  const [projects, setProjects] = useState([]);
  const history = useHistory();

  useEffect(() => {
    if (!accessToken) {
      history.push('/login');
      return;
    }

    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/tasks`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          return;
        }

        if (content.success) {
          setTasks(content.tasks);
        }
      });

    fetch(`${process.env.REACT_APP_API_URL}/projects`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          return;
        }

        if (content.success) {
          setProjects(content.projects);
        }
      });
  }, [showAddTaskForm, showAddProjectForm]);

  return (
    <div className="TasksPage">
      {showAddTaskForm ? <AddNewTaskForm onCloseButtonClicked={() => { setShowAddTaskForm(false); }} accessToken={accessToken} userId={userId} /> : ''}
      {showAddProjectForm ? <AddNewProjectForm onCloseButtonClicked={() => { setShowAddProjectForm(false); }} accessToken={accessToken} userId={userId} /> : ''}

      <div className={`page-content ${showAddTaskForm || showAddProjectForm ? 'blur' : ''}`}>
        <h1>Tasks</h1>
        <div className="progress-categories">
          <TaskStateCategory
            title="To do"
            onAddTaskButtonClicked={() => { setShowAddTaskForm(true); }}
            tasks={tasks ? (tasks.filter((task) => task.state === 'toDo')) : ''}
            accessToken={accessToken}
          />
          <TaskStateCategory
            title="In progress"
            onAddTaskButtonClicked={() => { setShowAddTaskForm(true); }}
            tasks={tasks ? (tasks.filter((task) => task.state === 'inProgress')) : ''}
            accessToken={accessToken}
          />
          <TaskStateCategory
            title="Done"
            onAddTaskButtonClicked={() => { setShowAddTaskForm(true); }}
            tasks={tasks ? (tasks.filter((task) => task.state === 'done')) : ''}
            accessToken={accessToken}
          />
          <div>
            <h2>
              Projects -
              {' '}
              {projects ? projects.length : '0'}
            </h2>
            {projects ? projects.map((project) => <ProjectItem key={project.id} project={project} tasks={tasks} />) : ''}
            <button type="submit" onClick={() => { setShowAddProjectForm(true); }}>+ Add project</button>
          </div>
        </div>
      </div>
    </div>
  );
}
