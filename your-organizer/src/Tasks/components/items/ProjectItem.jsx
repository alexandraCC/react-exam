import React, { useState, useEffect } from 'react';

export default function ProjectItem({ project, tasks }) {
  const [projectProgress, setProjectProgress] = useState(0);

  useEffect(() => {
    const tasksForProject = tasks.filter((task) => task.projectId === project.id);
    const doneTasksForProject = tasksForProject.filter((task) => task.state === 'done');

    if (doneTasksForProject.length === 0 || tasksForProject.length === 0) {
      return;
    }

    setProjectProgress(Math.round((doneTasksForProject.length / tasksForProject.length) * 100));
  }, [tasks]);

  return (
    <div className="Project">
      <p className="title">{project ? project.name : ''}</p>
      <p>
        {projectProgress}
        %
      </p>
    </div>
  );
}
