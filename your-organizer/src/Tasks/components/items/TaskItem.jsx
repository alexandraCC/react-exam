import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import '../../tasks.scss';

export default function TaskItem({ task, showStatus, accessToken }) {
  const [assignee, setAssignee] = useState(undefined);
  const history = useHistory();

  useEffect(() => {
    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    if (!task.assigneeId) {
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/users/${task.assigneeId}`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          return;
        }

        if (content.success) {
          setAssignee(content.user);
        }
      });
  }, []);

  return (
    // eslint-disable-next-line jsx-a11y/click-events-have-key-events
    <div className="TaskItem" role="button" onClick={() => history.push(`task/${task.id}`)} tabIndex="0">
      <p className="title">{task.title}</p>
      <p className="assignee">{ assignee ? assignee.nickName : '' }</p>
      {showStatus ? <p className="task-state">{task.state === 'toDo' ? 'To do' : task.state === 'inProgress' ? 'In progress' : 'Done'}</p> : ''}
    </div>
  );
}
