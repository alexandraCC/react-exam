import React, { useState } from 'react';
import '../../tasks.scss';

export default function AddNewProjectForm({ onCloseButtonClicked, accessToken }) {
  const [name, setName] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [successMessage, setSuccessMessage] = useState('');

  const addProject = (event) => {
    event.preventDefault();
    if (!name) {
      setErrorMessage('Please fill in a name!');
      return;
    }

    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/projects`, {
      method: 'POST',
      headers,
      body: JSON.stringify({
        name,
      }),
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          setErrorMessage(content.errorMessage);
          return;
        }

        if (content.success) {
          setSuccessMessage('Project was successfully created!');
          setName('');
          onCloseButtonClicked();
        }
      });
  };

  return (
    <form className="AddNewProjectForm form" onSubmit={addProject}>
      <div>
        <button type="button" className="close-button" onClick={onCloseButtonClicked}>x</button>
        <h3>Add a new project</h3>
      </div>

      {errorMessage ? <div className="error">{errorMessage}</div> : ''}
      {successMessage ? <div className="success">{successMessage}</div> : ''}

      <input type="text" id="title" placeholder="Name" value={name} onChange={(e) => setName(e.target.value)} />

      <button type="submit" className="is-bright" onClick={addProject}>Add</button>
    </form>
  );
}
