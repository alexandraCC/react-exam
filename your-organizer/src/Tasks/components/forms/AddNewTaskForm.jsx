import React, { useState, useEffect } from 'react';
import '../../tasks.scss';

export default function AddNewTaskForm({ onCloseButtonClicked, accessToken, userId }) {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [assignee, setAssignee] = useState('');
  const [project, setProject] = useState('');
  const [deadline, setDeadline] = useState(undefined);
  const [assignees, setAssignees] = useState(undefined);
  const [projects, setProjects] = useState(undefined);
  const [errorMessage, setErrorMessage] = useState('');
  const [successMessage, setSuccessMessage] = useState('');

  useEffect(() => {
    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/projects`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          return;
        }

        if (content.success) {
          setProjects(content.projects);
        }
      });

    fetch(`${process.env.REACT_APP_API_URL}/users`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          return;
        }

        if (content.success) {
          setAssignees(content.users);
        }
      });
  }, []);

  const addTask = (event) => {
    event.preventDefault();
    if (!title || !description) {
      setErrorMessage('Please fill in at least a title and description!');
      return;
    }

    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/tasks`, {
      method: 'POST',
      headers,
      body: JSON.stringify({
        title,
        description,
        assigneeId: assignee || null,
        projectId: project || null,
        deadline: deadline ? new Date(deadline).getTime() : null,
        state: 'toDo',
        userCreatedId: userId,
      }),
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          setErrorMessage(content.errorMessage);
          return;
        }

        if (content.success) {
          setSuccessMessage('Task was successfully created!');
          setTitle('');
          setDescription('');
          setAssignee('');
          setProject('');
          setDeadline(undefined);
          onCloseButtonClicked();
        }
      });
  };

  return (
    <form className="AddNewTaskForm form" onSubmit={addTask}>
      <div>
        <button type="button" className="close-button" onClick={onCloseButtonClicked}>x</button>
        <h3>Add a new task</h3>
      </div>

      {errorMessage ? <div className="error">{errorMessage}</div> : ''}
      {successMessage ? <div className="success">{successMessage}</div> : ''}

      <input type="text" id="title" placeholder="Title" value={title} onChange={(e) => setTitle(e.target.value)} />
      <textarea name="description" id="description" cols="30" rows="10" placeholder="Description" value={description} onChange={(e) => setDescription(e.target.value)} />
      <input type="date" id="deadline" placeholder="Deadline" value={deadline} onChange={(e) => setDeadline(e.target.value)} />

      <select name="assignee" id="assignee" value={assignee} onChange={(e) => setAssignee(e.target.value)}>
        <option value="">Assignee</option>
        {!assignees ? '' : assignees.map((element) => <option value={element.id} key={element.id}>{`${element.nickName} - ${element.firstName} ${element.lastName}` }</option>)}
      </select>

      <select name="project" id="project" value={project} onChange={(e) => setProject(e.target.value)}>
        <option value="">Project</option>
        {!projects ? '' : projects.map((element) => <option value={element.id} key={element.id}>{element.name}</option>)}
      </select>

      <button type="submit" className="is-bright" onClick={addTask}>Add</button>
    </form>
  );
}
