import React from 'react';
import TaskItem from './items/TaskItem';

export default function TaskStateCategory({
  title, onAddTaskButtonClicked, tasks, accessToken,
}) {
  return (
    <div className="TaskStateCategory">
      <h2>{`${title} – ${tasks.length}`}</h2>

      <div>
        {tasks
          ? (tasks.map((task) => <TaskItem task={task} showStatus={false} key={task.id} accessToken={accessToken} />))
          : ''}
      </div>

      {title === 'To do'
        ? <button type="submit" onClick={onAddTaskButtonClicked}>+ Add task</button>
        : ''}
    </div>
  );
}
