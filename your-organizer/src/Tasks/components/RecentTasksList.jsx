import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import TaskItem from './items/TaskItem';

export default function RecentTasksList({ accessToken }) {
  const history = useHistory();
  const [tasks, setTasks] = useState();

  useEffect(() => {
    const headers = new Headers();
    headers.append('Authorization', `Bearer ${accessToken}`);

    fetch(`${process.env.REACT_APP_API_URL}/tasks`, {
      method: 'GET',
      headers,
    }).then((response) => response.json())
      .then((content) => {
        if (content.error) {
          return;
        }

        if (content.success) {
          setTasks(content.tasks);
        }
      });
  }, []);

  return (
    <div className="RecentTasksList">
      <h2>Team Tasks</h2>

      <div>
        {tasks ? (tasks.slice(0, 4).map((task) => <TaskItem task={task} key={task.id} accessToken={accessToken} showStatus />)) : ''}
      </div>

      <button type="submit" onClick={() => history.push('/tasks')}>See all</button>
    </div>
  );
}
