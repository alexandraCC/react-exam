const router = require('express').Router();
const connection = require('../database');
const authenticateToken = require('../authenticateToken');

router.get('/notes', authenticateToken, (req, res) => {
	const userId = parseInt(req.query.userId, 10);

	connection.query(
		'SELECT * FROM notes WHERE userId=? ORDER BY timeCreated DESC',
		userId,
		async function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
			}

			return res.status(200).send({ success: true, notes: results });
		});
});

router.get('/notes/:id', authenticateToken, (req, res) => {
	const id = parseInt(req.params.id, 10);

	connection.query(
		'SELECT * FROM notes WHERE id = ?',
		id,
		async function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
			}

			return res.status(200).send({ success: true, note: results[0] });
		});
});

router.post('/notes', authenticateToken, (req, res) => {
	const newNote = req.body;

	connection.query(
		'INSERT INTO notes SET ?',
		{
			id: null,
			title: newNote.title,
			content: newNote.content,
			timeCreated: new Date().getTime(),
			userId: newNote.userId,
		},
		async function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Could not create a new note!' });
			}

			return res.status(200).send({ success: true, noteId: results.insertId });
		});
});

router.patch('/notes/:id', authenticateToken, (req, res) => {
	const updatedNote = req.body;
	const id = parseInt(req.params.id, 10);

	connection.query(
		'UPDATE notes SET title=?, content=? WHERE id=?',
		[updatedNote.title, updatedNote.content, id],
		async function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Could not update the note!' });
			}

			return res.status(200).send({ success: true, noteId: results });
		});
});

router.delete('/notes/:id', authenticateToken, (req, res) => {
	const id = parseInt(req.params.id, 10);

	connection.query(
		'DELETE FROM notes WHERE id = ?',
		id,
		async function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
			}

			return res.status(200).send({ success: true });
		});
});

module.exports = router;

