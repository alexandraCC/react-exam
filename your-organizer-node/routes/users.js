const router = require('express').Router();
const connection = require('../database');
const authenticateToken = require('../authenticateToken');

router.get('/users', authenticateToken, (req, res) => {
	connection.query(
		'SELECT * FROM users ORDER BY id DESC',
		async function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
			}

			return res.status(200).send({ success: true, users: results });
		});
});

router.get('/users/:id', authenticateToken, (req, res) => {
	const id = parseInt(req.params.id, 10);

	connection.query(
		'SELECT * FROM users WHERE id = ?',
		id,
		function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
			}

			return res.status(200).send({ success: true, user: results[0] });
		});
});

module.exports = router;

