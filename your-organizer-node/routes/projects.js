const router = require('express').Router();
const connection = require('../database');
const authenticateToken = require('../authenticateToken');

router.get('/projects', authenticateToken, (req, res) => {
	connection.query(
		'SELECT * FROM projects ORDER BY id DESC',
		async function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
			}

			return res.status(200).send({ success: true, projects: results });
		});
});

router.post('/projects', authenticateToken, (req, res) => {
	const newProject = req.body;

	connection.query(
		'INSERT INTO projects SET ?',
		{
			id: null,
			name: newProject.name,
		},
		function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Could not create a new project!' });
			}

			return res.status(200).send({ success: true, taskId: results.insertId });
		});
});

module.exports = router;

