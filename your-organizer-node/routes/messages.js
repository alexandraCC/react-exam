const router = require('express').Router();
const connection = require('../database');
const authenticateToken = require('../authenticateToken');

router.get('/messages', authenticateToken, (req, res) => {
	const senderUserId = parseInt(req.query.senderUserId, 10);
	const receiverUserId = parseInt(req.query.receiverUserId, 10);
	const idsArray = [senderUserId, receiverUserId];

	connection.query(
		'SELECT * FROM messages WHERE senderUserId IN (?) AND receiverUserId IN (?) ORDER BY timeSent ASC',
		[idsArray, idsArray],
		async function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
			}

			return res.status(200).send({ success: true, messages: results });
		});
});

module.exports = router;
