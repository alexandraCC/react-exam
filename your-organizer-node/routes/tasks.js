const router = require('express').Router();
const connection = require('../database');
const authenticateToken = require('../authenticateToken');

router.get('/tasks', authenticateToken, (req, res) => {
	connection.query(
		'SELECT * FROM tasks ORDER BY timeCreated DESC',
		 function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
			}

			return res.status(200).send({ success: true, tasks: results });
		});
});

router.get('/tasks/:id', authenticateToken, (req, res) => {
	const id = parseInt(req.params.id, 10);

	connection.query(
		'SELECT * FROM tasks WHERE id = ?',
		id,
		function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
			}

			return res.status(200).send({ success: true, task: results[0] });
		});
});

router.get('/tasks/dashboard-count/:userId', authenticateToken, (req, res) => {
	const userId = parseInt(req.params.userId, 10);

	connection.query(
		'SELECT * FROM tasks WHERE assigneeId = ?',
		userId,
		function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
			}

			const now = new Date();
			const todayTimeStamp = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1).getTime();

			const deadlinesToday = results.filter((task) => task.deadline !== null && task.deadline <= todayTimeStamp);
			const todos = results.filter((task) => task.state === 'toDo');
			const ongoing = results.filter((task) => task.state === 'inProgress');
			const done = results.filter((task) => task.state === 'done');

			return res.status(200).send({
				success: true,
				deadlinesToday: deadlinesToday.length,
				todos: todos.length,
				ongoing: ongoing.length,
				done: done.length
			});
		});
});

router.post('/tasks', authenticateToken, (req, res) => {
  const newTask = req.body;

	connection.query(
		'INSERT INTO tasks SET ?',
		{
			id: null,
			title: newTask.title,
			description: newTask.description,
			state: newTask.state,
			deadline: newTask.deadline ?? null,
			timeCreated: new Date().valueOf(),
			assigneeId: newTask.assigneeId ? newTask.assigneeId : null,
			projectId: newTask.projectId ? newTask.projectId : null,
			userCreatedId: newTask.userCreatedId,
		},
		function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Could not create a new task!' });
			}

			return res.status(200).send({ success: true, taskId: results.insertId });
		});
});

router.patch('/tasks/:id', authenticateToken, (req, res) => {
	const updatedTask = req.body;
	const id = parseInt(req.params.id, 10);

	connection.query(
		'UPDATE tasks SET title=?, description=?, state=?, deadline=?, assigneeId=?, projectId=? WHERE id=?',
		[updatedTask.title, updatedTask.description, updatedTask.state, updatedTask.deadline, updatedTask.assigneeId, updatedTask.projectId, id],
		async function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Could not update task!' });
			}

			return res.status(200).send({ success: true, noteId: results });
		});});

router.delete('/tasks/:id', authenticateToken, (req, res) => {
	const id = parseInt(req.params.id, 10);

	connection.query(
		'DELETE FROM tasks WHERE id = ?',
		id,
		function (error, results, fields) {
			if (error) {
				console.log(error);
				return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
			}

			return res.status(200).send({ success: true });
		});
});

module.exports = router;

