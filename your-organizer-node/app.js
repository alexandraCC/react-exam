require('dotenv').config();
const express = require('express');

const rateLimiter = require('express-rate-limit');
const https = require('https');
const http = require('http');
const fs = require('fs');

const connection = require('./database');
const authRoutes = require('./routes/auth');
const tasksRoutes = require('./routes/tasks');
const notesRoutes = require('./routes/notes');
const projectsRoutes = require('./routes/projects');
const usersRoutes = require('./routes/users');
const messagesRoutes = require('./routes/messages');

const app = express();
const port = process.env.PORT || 443;
let server;

if (port === 443) {
	server = https.createServer({
		key: fs.readFileSync('./server-key.pem'),
		cert: fs.readFileSync('./server-cert.pem')
	}, app);
} else {
	server = http.createServer(app);
}

const io = require('socket.io')(server, {
	cors: {
		origin: '*',
	}
});

// body parsers
app.use(express.json({ type: ['application/json', 'text/plain'] }));

// CORS headers
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS, PATCH');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, authorization, Content-Type, Accept');
	next();
});

// routes files
app.use(authRoutes);
app.use(notesRoutes);
app.use(tasksRoutes);
app.use(projectsRoutes);
app.use(usersRoutes);
app.use(messagesRoutes);

// rate limiting for auth routes
app.use('/auth', rateLimiter({
	windowMs: 5 * 60 * 1000, // 5 minutes
	max: 5 // limit each IP to 5 requests per 5 minutes
}));

io.on('connection', (socket) => {
	const socketId = socket.handshake.query.senderUserId;
	socket.join(socketId);

	socket.on('send-message', ({ message, receiverUserId }) => {
		receiverUserId = receiverUserId.toString();
		addMessageToDb(message, socketId, receiverUserId);
		io.to(receiverUserId).emit('receive-message', { text: message, senderId: socketId });
	});
});

const addMessageToDb = (text, senderUserId, receiverUserId) => {
	connection.query(
		'INSERT INTO messages SET ?',
		{
			id: null,
			text,
			senderUserId,
			receiverUserId,
			timeSent: new Date().getTime(),
		},
		async function (error, results, fields) {
			if (error) {
				console.log(error);
				return 'Could not create a new message!';
			}

			return results.insertId;
		});
};

server.listen(port, (error) => {
	if (error) {
		console.log('Error occurred: ' + error);
	}

	console.log('Server running on port ', port);
});
