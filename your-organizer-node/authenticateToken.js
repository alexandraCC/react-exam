const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
	const authHeader = req.headers['authorization'];
	const token = authHeader && authHeader.split(' ')[1];

	jwt.verify(token, process.env['ACCESS_SECRET_TOKEN'], (err, user) => {
		if (err) {
			return res.status(403);
		}

		req.user = user;
		next();
	});
}
